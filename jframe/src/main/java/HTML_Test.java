/**
 * @program: jframe
 * @description:
 * @author: Noel
 * @create: 2022-09-23 22:45
 **/
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class HTML_Test {
    public static void main(String[] args) {
        JFrame f = new JFrame("HTML test");
        JLabel lb = new JLabel("<html><h1>h1</h1></html>");
        JTextArea textArea = new JTextArea();
        JButton jButton = new JButton("submit");

        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lb.setText(textArea.getText());
            }
        });

        f.add(lb);
        f.add(textArea);
        f.add(jButton);



        f.setLayout(new GridLayout(8,1));
        f.setSize(200,200);
        f.setVisible(true);
    }
}