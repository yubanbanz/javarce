package noel.controller;

import org.apache.tomcat.util.descriptor.web.FilterDef;
import org.springframework.boot.web.embedded.tomcat.TomcatEmbeddedWebappClassLoader;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletRequest;

@Controller
public class IndexController {

    @ResponseBody
    @RequestMapping("/index")
    public String index(){

        return "hello";
    }

    @RequestMapping("/change")
    public String change(ServletRequest request){
        return "";
    }

}
