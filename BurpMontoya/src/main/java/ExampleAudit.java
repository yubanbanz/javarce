import burp.api.montoya.http.HttpService;
import burp.api.montoya.http.message.MarkedHttpRequestResponse;
import burp.api.montoya.scanner.audit.issues.AuditIssue;
import burp.api.montoya.scanner.audit.issues.AuditIssueConfidence;
import burp.api.montoya.scanner.audit.issues.AuditIssueDefinition;
import burp.api.montoya.scanner.audit.issues.AuditIssueSeverity;

import java.util.Arrays;
import java.util.List;

/**
 * @program: BurpMontoya
 * @description:
 * @author: Noel
 * @create: 2022-09-21 01:35
 **/
public class ExampleAudit implements AuditIssue {
    private String name;
    private String detail;
    private HttpService httpService;
    private MarkedHttpRequestResponse[] requestResponses;
    private AuditIssueConfidence confidence;
    private AuditIssueSeverity severity;
    private String baseurl;

    public ExampleAudit(String name, String detail, HttpService httpService, MarkedHttpRequestResponse[] requestResponses, AuditIssueSeverity severity, AuditIssueConfidence confidence, String baseurl){
        this.name = name;
        this.detail = detail;
        this.httpService = httpService;
        this.requestResponses = requestResponses;
        this.severity = severity;
        this.confidence = confidence;
        this.baseurl = baseurl;
    }



    @Override
    public String name() {
        return name;
    }

    @Override
    public String detail() {
        return detail;
    }

    @Override
    public String remediation() {
        return null;
    }

    @Override
    public HttpService httpService() {
        return httpService;
    }

    @Override
    public String baseUrl() {
        return baseurl;
    }

    @Override
    public AuditIssueSeverity severity() {
        return severity;
    }

    @Override
    public AuditIssueConfidence confidence() {
        return confidence;
    }

    @Override
    public List<MarkedHttpRequestResponse> requestResponses() {
        return Arrays.asList(requestResponses);
    }

    @Override
    public AuditIssueDefinition definition() {
        return null;
    }

}
