import burp.api.montoya.BurpExtension;
import burp.api.montoya.MontoyaApi;

import javax.swing.*;
import java.awt.*;

public class BurpExample implements BurpExtension {
    public static MontoyaApi montoyaApi;


    @Override
    public void initialize(MontoyaApi montoyaApi) {
        this.montoyaApi = montoyaApi;
        montoyaApi.misc().setExtensionName("Montoya");
        montoyaApi.userInterface().registerSuiteTab("Montoya", new MySuiteTab());
        montoyaApi.scanner().registerScanCheck(new FastjsonCheck());
    }


    private class MySuiteTab extends JComponent
    {
        public MySuiteTab()
        {
            JPanel customTabContent = new JPanel();
            customTabContent.setName("The One Ring Custom Tab Panel");
            customTabContent.setBackground(Color.GRAY);

            JButton button = new JButton("Print filename to log file");

            button.addActionListener(e -> {
                montoyaApi.logging().logToOutput("Montoya API used to log:" + montoyaApi.misc().extensionFilename());
            });

            customTabContent.add(button);
            add(customTabContent);
        }
    }
}
