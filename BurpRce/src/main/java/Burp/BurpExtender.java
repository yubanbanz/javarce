package Burp;

import Burp.Ui.Demo;
import burp.api.montoya.BurpExtension;
import burp.api.montoya.MontoyaApi;

import javax.swing.*;

public class BurpExtender implements BurpExtension {
    private  MontoyaApi montoyaApi;

    @Override
    public void initialize(MontoyaApi api) {
        this.montoyaApi = api;
        montoyaApi.misc().setExtensionName("BurpRce");
        montoyaApi.userInterface().registerSuiteTab("BurpRce",new Demo());
    }

    private class MySuiteTab extends JPanel
    {
        public MySuiteTab()
        {
            JPanel customTabContent = new JPanel();
            customTabContent.setName("The One Ring Custom Tab Panel");


            JButton button = new JButton("Print filename to log file");

            button.addActionListener(e -> {
                montoyaApi.logging().logToOutput("Montoya API used to log:" + montoyaApi.misc().extensionFilename());
            });

            customTabContent.add(button);
            add(customTabContent);
//            setVisible(true);
        }
    }
}
