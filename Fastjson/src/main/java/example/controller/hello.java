package example.controller;

import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class hello {

    @ResponseBody
    @RequestMapping("/say")
    public String say(String word){
        JSON.parseObject(word);
        return "hello!";
    }

}
